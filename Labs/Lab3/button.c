#include <stdio.h>
#include <stdlib.h>
#include "mraa.h"

#define up 47
#define down 44
#define left 165
#define right 45
#define select 48
#define a 49
#define b 46

#define PRESS_THRESHOLD 0x3F
#define RELEASE_THRESHOLD 0xFC

/*
 * Lab 3 - button.c
 *
 * By Joseph Shin, Aaron Carson, and Jason Gersztyn
 */

static mraa_gpio_context aPin = NULL;
uint8_t btn = 1;

// handle naive implementation of button listener (without interrupts).
void pushButton();

// entry point.
int main()
{
	pushButton();
	return 0;
}

// main code for button.c
void pushButton()
{
	aPin = mraa_gpio_init_raw(a);
	mraa_gpio_dir(aPin,MRAA_GPIO_IN);
	int read = 0;

	for(;;)
	{
		usleep(5000);
		static uint8_t shiftReg = 0xFF;
		shiftReg >>= 1;
		read = mraa_gpio_read(aPin);

		if(read == 1)
		{
			shiftReg |= (1 << 7);
		}
		if(btn == 0)
		{
			if(shiftReg >= RELEASE_THRESHOLD)
			{
				btn = 1;
				printf("Button A is not pressed\n");
			}
		}
		else
		{
			if(shiftReg <= PRESS_THRESHOLD)
			{
				btn = 0;
				printf("Button A is pressed\n");
			}
		}
	}
}