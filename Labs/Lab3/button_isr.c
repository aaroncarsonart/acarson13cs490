#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "mraa.h"

#define up 47
#define down 44
#define left 165
#define right 45
#define select 48
#define a 49
#define b 46

#define PRESS_THRESHOLD 0x3F
#define RELEASE_THRESHOLD 0xFC

/*
 * Lab 3 - button_isr.c
 *
 * By Joseph Shin, Aaron Carson, and Jason Gersztyn
 */

static mraa_gpio_context aPin = NULL;
static mraa_gpio_context x = NULL;
uint8_t btn = 1;

// code to initialize the button listening for isr.
void isr();

// the interrupt function with the required void pointer.
void interrupt(void*);

// the main method.
int main()
{
	isr();
	return 0;
}

void interrupt(void* args)
{
	for(;;)
	{
		int read = 0;
		static uint8_t shiftReg = 0xFF;
		shiftReg >>= 1;
		read = mraa_gpio_read(x);

		if(read == 1)
		{
			shiftReg |= (1 << 7);
		}
		if(btn == 0)
		{
			if(shiftReg >= RELEASE_THRESHOLD)
			{
				btn = 1;
				printf("Button A is not pressed\n");
			}
		}
		else
		{
			if(shiftReg <= PRESS_THRESHOLD)
			{
				btn = 0;
				printf("Button A is pressed\n");
			}
		}
		usleep(5000);
	}
}

void isr()
{
	mraa_init();
	//mraa_gpio_context x = mraa_gpio_init_raw(a);
	x = mraa_gpio_init_raw(a);
	mraa_gpio_dir(x, MRAA_GPIO_IN);
	mraa_gpio_edge_t edge = MRAA_GPIO_EDGE_BOTH;
	mraa_gpio_isr(x,edge,&interrupt,NULL);

	for(;;)
	{
		// empty cuz interrupts
	}
	mraa_gpio_close(x);
}