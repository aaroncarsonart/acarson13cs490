//============================================================================
// Name        : Lab4.cpp
// Author      : Joseph Shin, Jason Gersztyn, Aaron Carson

// Description : Implementation of Lab 4 with Sparkfun OLED libraries
//============================================================================
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "mraa.hpp"
#include "oled/Edison_OLED.h"  // for the sparkfun OLED libraries

#define UP 47
#define DOWN 44
#define LEFT 165
#define RIGHT 45
#define SELECT 48
#define BUTTON_A 49
#define BUTTON_B 46

#define PRESS_THRESHOLD 0x3F
#define RELEASE_THRESHOLD 0xFC

#define XM_ADDR 0x1D
#define GYRO_ADDR 0x6B

#define OUT_X_L_M 0x08	// magnetometer start address
#define OUT_X_H_M 0x09
#define OUT_Y_L_M 0xA0
#define OUT_Y_H_M 0xA1
#define OUT_Z_L_M 0xA2
#define OUT_Z_H_M 0xA3

#define OUT_X_L_G 0x28	// gyroscope start address		// both of these need a mode switch
#define OUT_X_L_A 0x28	// accelerometer start address	// or different setting for the i2c

#define OUT_TEMP_L_XM 0x05
#define OUT_TEMP_H_XM 0x06

#define CTRL_REG1_G 0x20
#define CTRL_REG2_G 0x21
#define CTRL_REG3_G 0x22
#define CTRL_REG4_G 0x23
#define CTRL_REG5_G 0x24

#define CTRL_REG0_XM 0x1F
#define CTRL_REG1_XM 0x20
#define CTRL_REG2_XM 0x21
#define CTRL_REG3_XM 0x22
#define CTRL_REG4_XM 0x23
#define CTRL_REG5_XM 0x24
#define CTRL_REG6_XM 0x25
#define CTRL_REG7_XM 0x26

#define INT_CTRL_REG_M 0x12

// --------------------------------------------
// helper struct to hold the temperature in
// celcius and fahrenheit.
// --------------------------------------------
typedef struct {
	float c;
	float f;
} Temp;

// --------------------------------------------
// contains all functions for Completing 4.
// --------------------------------------------
class Lab4 {
public:
	// helper functions
	static void tempRead(Temp*);
	static void printTemperature(Temp*);
	static void quitHandlerOnTerminal(int);
	static void quitHandlerOnOLED(int);
	static void initGyro();
	static void initMag();
	static void initAcc();

	// shared fields
	static mraa::I2c* i2c;
	static edOLED oled;

	// main functions
	static void displayTempOnTerminal();  // question 1
	static void displayTempOnOLED();      // question 2
	static void displayOnTerminal9DOF();  // question 3
	static void displayOnOLED_9DOF();     // question 4
};

// entry point for i2c operations.
mraa::I2c* Lab4::i2c;

// entry point for the Sparkfun OLED
edOLED Lab4::oled;

// --------------------------------------------
// MAIN
// --------------------------------------------
int main() {
	//Lab4::displayTempOnTerminal();
	//Lab4::displayTempOnOLED();
	//Lab4::displayOnTerminal9DOF();
	Lab4::displayOnOLED_9DOF();
	return 0;
}

void Lab4::quitHandlerOnTerminal(int signal) {
	delete Lab4::i2c;
	printf("Exit with terminal quit handler.\n");
	exit(EXIT_SUCCESS);
}

void Lab4::quitHandlerOnOLED(int signal) {
	Lab4::oled.clear(PAGE);
	Lab4::oled.display();
	printf("Exit with OLED quit handler.\n");
	exit(EXIT_SUCCESS);
}

// --------------------------------------------
// Helper Function to read the temperature.
// --------------------------------------------
void Lab4::tempRead(Temp * temp) {

	Lab4::i2c = new mraa::I2c(1);
	mraa_result_t return_value = i2c->address(XM_ADDR);
	uint8_t buffer[2];

	// if read successfully, continue with everything.
	if (return_value == MRAA_SUCCESS) {

		// tell it what mode to use, setting TEMP_EN to 1 (1st bit)
		i2c->writeReg(CTRL_REG5_XM, 0b10000000);

		//read from the register
		i2c->address(XM_ADDR);
		buffer[0] = i2c->readReg(OUT_TEMP_H_XM);
		i2c->address(XM_ADDR);
		buffer[1] = i2c->readReg(OUT_TEMP_L_XM);

		//char c = i2c -> readReg(CTRL_REG5_XM);

		int16_t raw_temperature = buffer[0] << 8 | buffer[1];
		float celcius = raw_temperature / 8.0 + 21; // 8 lsb per C˚ + an offset scaling factor
		float fahrenheit = celcius * 9.0 / 5.0 + 32; //converting to fahrenheit

		temp->c = celcius;
		temp->f = fahrenheit;
	} else {
		temp->c = 0;
		temp->f = 0;
	}
}

// --------------------------------------------
// Helper function to print temp data.
// --------------------------------------------
void Lab4::printTemperature(Temp * t) {
	printf("Temperature:  %.2f C˚  %.2f F˚ \n", t->c, t->f);

}

// ############################################################################
// Question 1.
// ############################################################################

// ---------------------------------------------------
// Continually print the temperature to the terminal.
// ---------------------------------------------------
void Lab4::displayTempOnTerminal() {

	// register SIGINT handler
	signal(SIGINT, quitHandlerOnTerminal);

	Temp temp;
	for (;;) {
		tempRead(&temp);
		printTemperature(&temp);
		usleep(50000);
	}
}

// ############################################################################
// Question 2.
// ############################################################################

// --------------------------------------------
// completing Question 2 of Lab 4.
// display on OLED screen.
// --------------------------------------------
void Lab4::displayTempOnOLED() {

	// register SIGINT handler
	signal(SIGINT, quitHandlerOnOLED);

	// initialize the OLED display
	oled.begin();
	oled.clear(ALL);
	oled.display();

	// create our string to display the temperature with.
	char *tempOut = (char*) malloc(64 * sizeof(char));

	// create the Temp struct to hold our temperature data.
	Temp temp;

	for (;;) {
		oled.setCursor(1, 1);

		// get the current temperature data.
		tempRead(&temp);
		//Lab4::printTemperature(&temp);  // print data to terminal also

		// format the string.
		sprintf(tempOut, "Temp:\n\n%.2f C\n%.2f F", temp.c, temp.f);

		// display the string.
		oled.clear(PAGE);
		oled.print(tempOut);
		oled.display();
		usleep(50000);
	}

}

// ---------------------------------------------------------------------------
// here are a set of structs to be used to hold the 9 degrees of freedom data.
// ---------------------------------------------------------------------------
typedef struct {
	float x;
	float y;
	float z;
} accelerometer;

typedef struct {
	float x;
	float y;
	float z;

} gyroscope;

typedef struct {
	float x;
	float y;
	float z;
} magnetometer;

typedef struct {
	accelerometer a;
	gyroscope g;
	magnetometer m;
} nineDOF;

typedef struct {
	int16_t x;
	int16_t y;
	int16_t z;
} raw_data;

typedef struct {
	raw_data a;
	raw_data g;
	raw_data m;
} nineDOF_raw;


// ------------------------------------
// Init the I2c to read gyro data
// ------------------------------------
void Lab4::initGyro() {
	i2c->address(GYRO_ADDR);
	i2c->writeReg(CTRL_REG1_G, 0x0f);
	i2c->writeReg(CTRL_REG2_G, 0x00);
	i2c->writeReg(CTRL_REG3_G, 0x88);
	i2c->writeReg(CTRL_REG4_G, 0x00);
	i2c->writeReg(CTRL_REG5_G, 0x00);
}

// ------------------------------------
// Init the I2c to read acc data
// ------------------------------------
void Lab4::initAcc() {
	i2c->address(XM_ADDR);
	i2c->writeReg(CTRL_REG0_XM, 0x00);
	i2c->writeReg(CTRL_REG1_XM, 0x57);
	i2c->writeReg(CTRL_REG2_XM, 0x00);
	i2c->writeReg(CTRL_REG3_XM, 0x04);
}

// ------------------------------------
// Init the I2c to read mag data
// ------------------------------------
void Lab4::initMag() {
	i2c->address(XM_ADDR);
	i2c->writeReg(CTRL_REG4_XM, 0x04);
	i2c->writeReg(CTRL_REG5_XM, 0x94);
	i2c->writeReg(CTRL_REG6_XM, 0x00);
	i2c->writeReg(CTRL_REG7_XM, 0x00);
	i2c->writeReg(INT_CTRL_REG_M, 0x09);
}

// ############################################################################
// Question 3.
// ############################################################################

// -----------------------------------------
// helper function read mag data
// -----------------------------------------
void readData(mraa::I2c * i2c, raw_data * raw_data, uint8_t reg) {
	uint8_t data[6];
	i2c->readBytesReg(reg | 0x80, data, 6);
	uint16_t x, y, z;

	x = (data[1] << 8 | data[0]);
	y = (data[3] << 8 | data[2]);
	z = (data[5] << 8 | data[4]);
	//printf("(x,y,z) (0x%0x, 0x%0x, 0x%0x)\n", x, y, z);
	//printf("(x,y,z) (%d,%d,%d)\n", x, y, z);

	raw_data->x = x;
	raw_data->y = y;
	raw_data->z = z;
}

const float GS_PER_LSB = 0.061 / 1000; // mg/LSB * g/1000mg
const float G_OFFSET = 3;             // offset to fix factory translation error

// -----------------------------------------------------
// Convert Acceleration reading from uint16_t to float.
// -----------------------------------------------------
float convertAcceleration(uint16_t acc_raw) {
	return acc_raw * GS_PER_LSB - G_OFFSET;
}

// -----------------------------------------------------
// Convert Gyroscope reading from uint16_t to float.
// -----------------------------------------------------
float convertGyro(uint16_t g_raw) {
	return g_raw;
}

const float GAUSS_PER_LSB = 0.08 / 1000; // mgauss/LSB * g/1000mg
const float GAUSS_OFFSET = 0;
// -----------------------------------------------------
// Convert Magnetometer reading from uint16_t to float.
// -----------------------------------------------------
float convertMag(uint16_t m_raw) {
	return m_raw - GAUSS_PER_LSB - GAUSS_OFFSET;
}

// --------------------------------------------
// Answering Question 3 of Lab 4.
// --------------------------------------------
void Lab4::displayOnTerminal9DOF() {
	signal(SIGINT, quitHandlerOnTerminal);
	Lab4::i2c = new mraa::I2c(1);
	nineDOF_raw n_raw;
	nineDOF n;
	//raw_data m_raw, a_raw, g_raw;
	for (;;) {

		// ---------------------------------------------------
		// read the raw data into the raw struct
		// ---------------------------------------------------
		initMag();
		readData(i2c, &n_raw.m, OUT_X_L_M);
		initAcc();
		readData(i2c, &n_raw.a, OUT_X_L_A);
		initGyro();
		readData(i2c, &n_raw.g, OUT_X_L_G);

		//printf(
		//		"x,y,z  mag:   %8d %8d %8d   acc:   %8d %8d %8d   gyro:   %8d %8d %8d\n",
		//		n_raw.m.x, n_raw.m.y, n_raw.m.z, n_raw.a.x, n_raw.a.y,
		//		n_raw.a.z, n_raw.g.x, n_raw.g.y, n_raw.g.z);

		// convert the data into human readable, scaled code.

		n.m.x = convertMag(n_raw.m.x);
		n.m.y = convertMag(n_raw.m.y);
		n.m.z = convertMag(n_raw.m.z);
		//printf("mag:  %3.3f   %3.3f  %3.3f   ", n.m.x, n.m.y, n.m.z);
		printf("mag:  %8d  %8d  %8d   ", n_raw.m.x, n_raw.m.y, n_raw.m.z);
		n.a.x = convertAcceleration(n_raw.a.x);
		n.a.y = convertAcceleration(n_raw.a.y);
		n.a.z = convertAcceleration(n_raw.a.z);
		printf("acc:  %2.3fg  %2.3fg  %2.3fg  ", n.a.x, n.a.y, n.a.z);
		n.g.x = convertGyro(n_raw.g.x);
		n.g.y = convertGyro(n_raw.g.y);
		n.g.z = convertGyro(n_raw.g.z);
		printf("gyro:  %3.3fg  %3.3fg  %3.3fg  ", n.g.x, n.g.y, n.g.z);
		printf("\n");

		usleep(500000);
	}

	//mraa_result_t result = i2c->address(XM_ADDR);
	//uint8_t buffer[2];
}

typedef struct {
	mraa::Gpio * gpio;
	uint8_t reg;
	bool pressed;
} button;

// ----------------------------------------------------------------------
// Question 4: Paging and OLED interface.
// ----------------------------------------------------------------------
void Lab4::displayOnOLED_9DOF() {
	signal(SIGINT, quitHandlerOnOLED);

	// declare and initialize values needed.
	i2c = new mraa::I2c(1);
	oled.begin();
	oled.clear(ALL);
	oled.display();

	button up;
	up.gpio = new mraa::Gpio(UP, true, true);
	button down;
	down.gpio = new mraa::Gpio(DOWN, true, true);

	// used for paging
	int page = 0;

	// hold all raw and coverted NDOF data
	nineDOF_raw n_raw;
	nineDOF n;
	Temp temp;

	char *output = (char*) malloc(64 * sizeof(char));
	for (;;) {

		// ----------------------------------------
		// STEP 1: read user input
		// ----------------------------------------
		if (up.gpio->read()) page = (page - 1) < 0 ? 4 : page - 1;
		if (down.gpio->read()) page = (page + 1) % 5;

		oled.clear(PAGE);
		oled.setCursor(0, 0);

		switch(page + 1){

		default:
		case 1:
			//Page 1: a welcome screen telling the user how to use this device
			oled.print("9DOF OLED\n\nPress up\nor down toscroll\nthe data.");
			break;

		case 2:
			//Page 2: current temperature in both Fahrenheit and Celsius (bonus points for showing maximum and minimum temperatures since the device was turned on)
			//oled.print("page 2");
			tempRead(&temp);
			sprintf(output, "Temp:\n\n%.2f C\n%.2f F", temp.c, temp.f);
			oled.print(output);
			break;


	    case 3:
	    	//Page 3: ax, ay, and az acceleration values in g's
			initAcc();
			readData(i2c, &n_raw.a, OUT_X_L_A);
			n.a.x = convertAcceleration(n_raw.a.x);
			n.a.y = convertAcceleration(n_raw.a.y);
			n.a.z = convertAcceleration(n_raw.a.z);
			sprintf(output, "Acc\n\n%2.3fg\n%2.3fg\n%2.3fg", n.a.x, n.a.y, n.a.z);
			oled.print(output);
	    	//oled.print("page 3");
			break;

		case 4:
			//Page 4: roll, pitch and yaw gyroscope values in degrees/second
			initGyro();
			readData(i2c, &n_raw.g, OUT_X_L_G);
			sprintf(output, "Gyro\n\nx: %d\ny: %d\nz: %d", n_raw.g.x,n_raw.g.y,n_raw.g.z);
			oled.print(output);
			//oled.print("page 4");
			break;
		case 5:
			//
			//Page 5: mx, my, and mz magnetometer values (optionally calibrated in milliGauss -- 250mG to 650mG at Earth's surface)
			initMag();
			readData(i2c, &n_raw.m, OUT_X_L_M);
			sprintf(output, "Mag\n\nx: %d\ny: %d\nz: %d", n_raw.m.x,n_raw.m.y,n_raw.m.z);
			oled.print(output);
			//oled.print("page 5");
			break;
		}

		oled.display();
		usleep(50000);
	}

}

