//============================================================================
// Name        : Lab5.c
// Author      : Joseph Shin, Jason Gersztyn, Aaron Carson

// Description : Implementation of Lab 5
//============================================================================
/**
 * @file Lab5.c
 * @brief Code created for completion of Lab 5.
 *
 * by Joseph Shin, Jason Gersztyn, Aaron Carson
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mraa.h"

#define MOTOR_1_DEG_PER_MICROSTEP 0.05625 // 0.9 degrees per step / 16 microsteps
#define MOTOR_2_DEG_PER_MICROSTEP 0.1125 // 0.9 degrees per step / 16 microsteps

// The stepper motor with a round platform.
#define MOTOR_1_STEP   7
#define MOTOR_1_DIR    6
#define MOTOR_1_ENABLE 4

// The stepper motor connected to the arm.
#define MOTOR_2_STEP   10
#define MOTOR_2_DIR    9
#define MOTOR_2_ENABLE 8

#define LASER_VMOD  3 // laser PWM pin
#define LASER_POWER 2 // MOSFET gate (use GPIO)
#define LED_POWER   5 // MOSFET gate (use PWM only for LED)

#define HIGH 1
#define LOW 0

#define CCW 1
#define CW 0


//===============================================================================
// 1. INTERFACE
//===============================================================================

//--------------------------------------------------------------------------
// StepperMotor
//--------------------------------------------------------------------------

/**
 * StepperMotor is a struct that holds needed data for operating the motor.
 */
struct StepperMotor {
	mraa_gpio_context step;
	mraa_gpio_context dir;
	mraa_gpio_context enable;

	float deg_per_microstep;
};

/**
 * Create a new struct StepperMotor.
 *
 * @param step_pin          The STEP pin used by the motor.
 * @param dir_pin           The DIR pin used by the motor.
 * @param enable_pin        The ENABLE used by the motor.
 * @param deg_per_microstep The length of the on time in microseconds.
 * @return                  A pointer to a new struct StepperMotor.
 */
struct StepperMotor * stepper_create(int step_pin, int dir_pin, int enable_pin,
		float deg_per_microstep);


/**
 * Create a struct StepperMotor struct for Motor 1, the motor with the round platform.
 * @return   A pointer to a new struct StepperMotor set up to control Motor 1.
 */
struct StepperMotor * stepper_create_motor1();

/**
 * Create a struct StepperMotor struct for Motor 2, the motor with the round platform.
 * @return   A pointer to a new struct StepperMotor set up to control Motor 2.
 */
struct StepperMotor * stepper_create_motor2();

/**
 * Move the motor a set number of steps, using the struct StepperMotor's current
 * period and on_time values.
 *
 * @param motor     Pointer to a struct StepperMotor struct
 * @param direction CW or CCW, the direction of the motor.
 * @param seconds   How many seconds to complete the operation.
 * @param degrees   How many degrees to move the motor.
 */
void stepper_move(struct StepperMotor * motor, int direction, float seconds,
		float degrees);

/**
 * Free all memory and release sources consumed by the StepperMotor.
 * @param motor The StepperMotor to free memory.
 */
void stepper_delete(struct StepperMotor * motor);


//--------------------------------------------------------------------------
// Laser
//--------------------------------------------------------------------------

/**
 * Laser is a struct that holds a gpio and pwm context, along with a
 * duty cycle reference to easy control of Laser parameters.
 */
struct Laser {
	mraa_gpio_context power;
	mraa_pwm_context vmod;
	float duty_cycle;
};

/**
 * Create a new Laser.
 * @return A pointer to a new Laser struct.
 */
struct Laser * laser_create();

/**
 * Delete the laser. (Free memory)
 * @param laser A pointer to the Laser struct to delete.
 */
void laser_delete(struct Laser * laser);

/**
 * Enable the laser, turning it on with current settings.
 * @param laser A pointer to a Laser struct.
 */
void laser_enable(struct Laser * laser);

/**
 * Disable the laser, turning it off.
 * @param laser A pointer to a Laser struct.
 */
void laser_disable(struct Laser * laser);

/**
 * Set the period of the Laser's pwm cycle.
 * @param laser A pointer to a Laser struct.
 * @param frequency The frequency from which to derive the period.
 */
void laser_set_period_us(struct Laser * laser, float frequency);

/**
 * Set the duty cycle of the Laser's pwm cycle.
 * @param laser A pointer to a Laser struct.
 * @param percentage The percentage to set the duty cycle, use values from 0 to 1.
 */
void laser_set_duty_cycle(struct Laser * laser, float percentage);

//--------------------------------------------------------------------------
// LED
//--------------------------------------------------------------------------

/**
 * LED is a struct that holds a pwm context and a duty cycle reference for
 * easy control of the LED.
 */
struct LED {
	mraa_pwm_context power;
	float duty_cycle;
};

/**
 * Create a new LED struct.
 * @return A pointer to a LED struct.
 */
struct LED * led_create();

/**
 * Delete a LED struct (Free the memory).
 * @param led A pointer to a LED struct.
 */
void led_delete(struct LED * led);

/**
 * Enable the LED, turning it on with current settings.
 * @param led The pointer to the LED struct.
 */
void led_enable(struct LED * led);

/**
 * Disable the LED, turning it off.
 * @param led The pointer to the LED struct.
 */
void led_disable(struct LED * led);

/**
 * Set the period in microseconds of the given LED.
 * @param led The pointer to the LED struct.
 * @param frequency The frequency from which to derive the period.
 */
void led_set_period_us(struct LED * led, float frequency);

/**
 * Set the duty cycle of the pwm used for the LED.
 * @param led The pointer to the LED struct.
 * @param percentage The percentage to set the duty cycle, use values from 0 to 1.
 */
void led_set_duty_cycle(struct LED * led, float percentage);

/**
 * Run a choreographed sequence of operations on the motors, LED and laser using
 * our interface.
 */
void choreograph();


//===============================================================================
// 2. MAIN
//===============================================================================

/**
 * Run our code for Lab5.
 *
 * @return Always returns zero.
 */
int main() {
	//time_stamp_example();
	choreograph();
	return 0;
}

//===============================================================================
// 3. IMPLEMENTATION
//===============================================================================


//--------------------------------------------------------------------------
// StepperMotor (implementation)
//--------------------------------------------------------------------------

struct StepperMotor * stepper_create(int step_pin, int dir_pin, int enable_pin,
		float deg_per_microstep) {
	struct StepperMotor * motor = (struct StepperMotor *) malloc(
			sizeof(struct StepperMotor));
	motor->step = mraa_gpio_init(step_pin);
	motor->dir = mraa_gpio_init(dir_pin);
	motor->enable = mraa_gpio_init(enable_pin);
	mraa_gpio_dir(motor->step, MRAA_GPIO_OUT);
	mraa_gpio_dir(motor->dir, MRAA_GPIO_OUT);
	mraa_gpio_dir(motor->enable, MRAA_GPIO_OUT);
	// TODO setup any input/output calls for step, dir, and enable.

	motor->deg_per_microstep = deg_per_microstep;
	return motor;
}

void stepper_delete(struct StepperMotor * motor) {
	mraa_gpio_close(motor->step);
	mraa_gpio_close(motor->dir);
	mraa_gpio_close(motor->enable);
	free(motor);
}


struct StepperMotor * stepper_create_motor1() {
	return stepper_create(MOTOR_1_STEP, MOTOR_1_DIR, MOTOR_1_ENABLE,
	MOTOR_1_DEG_PER_MICROSTEP);
}

struct StepperMotor * stepper_create_motor2() {
	return stepper_create(MOTOR_2_STEP, MOTOR_2_DIR, MOTOR_2_ENABLE,
	MOTOR_2_DEG_PER_MICROSTEP);
}

void stepper_move(struct StepperMotor * motor, int direction, float seconds,
		float degrees) {
	// write enable to LOW to activate motor
	mraa_gpio_write(motor->enable, LOW);
	mraa_gpio_write(motor->dir, direction);

	int steps = (int) (degrees / motor->deg_per_microstep);

	// sleep time per microstep -> period / 2
	float period_per_step = seconds * 1000000 / steps;
	int sleep_time = (int) (period_per_step / 2);

	int i;
	for (i = 0; i < steps; i++) {
		// set high, sleep for on time
		mraa_gpio_write(motor->step, HIGH);
		usleep(sleep_time);

		// set low, sleep for period - on time
		mraa_gpio_write(motor->step, LOW);
		usleep(sleep_time);
	}

	// turn off motor
	mraa_gpio_write(motor->enable, HIGH);
}

//--------------------------------------------------------------------------
// Laser (implementation)
//--------------------------------------------------------------------------

struct Laser * laser_create() {
	struct Laser * laser = (struct Laser *) malloc(sizeof(struct Laser));
	laser->power = mraa_gpio_init(LASER_POWER);
	laser->vmod = mraa_pwm_init(LASER_VMOD);
	laser->duty_cycle = 1;
	return laser;

}
void laser_delete(struct Laser * laser) {
	mraa_gpio_close(laser->power);
	mraa_pwm_close(laser->vmod);
	free(laser);
}

void laser_set_period_us(struct Laser * laser, float frequency) {
	int period_us =
			(int) (1000000 /*us per second*// frequency /*cycles per second*/);
	mraa_pwm_period_us(laser->vmod, period_us);

}

void laser_set_duty_cycle(struct Laser * laser, float percentage) {
	laser->duty_cycle = percentage;
	mraa_pwm_write(laser->vmod, percentage);
}

void laser_enable(struct Laser * laser) {
	mraa_gpio_write(laser->power, HIGH);
	mraa_pwm_enable(laser->vmod, HIGH);
	mraa_pwm_write(laser->vmod, laser->duty_cycle);

}

void laser_disable(struct Laser * laser) {
	mraa_pwm_write(laser->vmod, 0);
	mraa_pwm_enable(laser->vmod, LOW);
	mraa_gpio_write(laser->power, LOW);
}

//--------------------------------------------------------------------------
// LED (implementation)
//--------------------------------------------------------------------------

struct LED * led_create() {
	struct LED * led = (struct LED *) malloc(sizeof(struct LED));
	led->power = mraa_pwm_init(LED_POWER);
	led->duty_cycle = 1;

	return led;
}

void led_delete(struct LED * led) {
	mraa_pwm_close(led->power);
	free(led);
}

void led_enable(struct LED * led) {
	mraa_pwm_enable(led->power, HIGH);
	mraa_pwm_write(led->power, led->duty_cycle);
}

void led_disable(struct LED * led) {
	mraa_pwm_write(led->power, 0);
	mraa_pwm_enable(led->power, LOW);
}

void led_set_period_us(struct LED * led, float frequency) {
	int period_us =
			(int) (1000000 /*us per second*// frequency /*cycles per second*/);
	mraa_pwm_period_us(led->power, period_us);
}

void led_set_duty_cycle(struct LED * led, float percentage) {
	led->duty_cycle = percentage;
	mraa_pwm_write(led->power, percentage);
}


//--------------------------------------------------------------------------
// Choreograph (implementation)
//--------------------------------------------------------------------------

void choreograph() {
	// a short period means a faster motor.

	// steps controls how far it rotates.  Check documentation for degrees, not sure yet.	int steps = 1000;

	printf("start!\n");
	srand(time(NULL));

	//-------------------------------
	// 1. SETUP
	//-------------------------------

	// setup motors.
	struct StepperMotor * motor1 = stepper_create_motor1();
	struct StepperMotor * motor2 = stepper_create_motor2();

	// for loops
	int i;

	// make the motor 2 successively move faster.
	for (i = 0; i < 4; i++) {
		stepper_move(motor2, CW, 0.5, 15);
		stepper_move(motor2, CCW, 0.25, 15);
		stepper_move(motor2, CW, 0.125, 15);
		stepper_move(motor2, CCW, 0.06, 15);
		stepper_move(motor2, CW, 0.03, 15);
		stepper_move(motor2, CCW, 0.015, 15);
		stepper_move(motor2, CW, 0.007, 15);
	}

	// setup laser.
	struct Laser * laser = laser_create();
	laser_set_period_us(laser, 450 * 1000);

	// setup LED.
	struct LED * led = led_create();
	led_set_period_us(led, 450 * 1000);

	// for random stuff.
	float random;
	int r_limit = 30;

	// make both motors randomly move.
	for (i = 0; i < 30; i++) {
		random = (rand() % r_limit) / 100.0;
		stepper_move(motor1, CW, random, 15);
		random = (rand() % r_limit) / 100.0;
		stepper_move(motor1, CCW, random, 15);
	}

	for (i = 0; i < 30; i++) {
		random = (rand() % r_limit) / 100.0;
		stepper_move(motor2, CW, random, 15);
		random = (rand() % r_limit) / 100.0;
		stepper_move(motor2, CCW, random, 15);
	}

	// for sleeping on strobe
	int strobe_us = 200000;

	// flash the led on and off 10 times.
	for (i = 0; i < 20; i++) {
		if (i % 2 == 0)
			led_enable(led);
		else
			led_disable(led);
		usleep(strobe_us);
	}

	stepper_move(motor1, CCW, 1, 15);
	stepper_move(motor1, CW, 1, 30);
	stepper_move(motor1, CCW, 1, 45);
	stepper_move(motor1, CW, 1, 60);
	stepper_move(motor1, CCW, 1, 75);
	stepper_move(motor1, CW, 1, 90);
	stepper_move(motor1, CW, 1, 105);
	stepper_move(motor1, CW, 1, 120);

	// move the motors.
	stepper_move(motor1, CCW, 1, 180);

	// flash the laser on and off 10 times.
	for (i = 0; i < 20; i++) {
		if (i % 2 == 0)
			laser_enable(laser);
		else
			laser_disable(laser);
		usleep(strobe_us);
	}

	// manually manipulate mottor 2 speeds.
	stepper_move(motor2, CCW, 0.5, 360);
	stepper_move(motor2, CW, 0.5, 180);
	stepper_move(motor2, CCW, 1.0, 15);
	stepper_move(motor2, CW, 0.5, 15);
	stepper_move(motor2, CCW, 0.25, 15);
	stepper_move(motor2, CW, 0.125, 15);
	stepper_move(motor2, CCW, 0.06, 15);
	stepper_move(motor2, CW, 0.03, 15);
	stepper_move(motor2, CCW, 0.015, 15);
	stepper_move(motor2, CW, 0.007, 15);

	// turn on laser.
	laser_enable(laser);

	// 3 times
	for (i = 0; i < 3; i++) {

		// fade laser on and off
		float duty_cycle;
		float step = 0.0001;
		for (duty_cycle = 0; duty_cycle <= 1; duty_cycle += step) {
			laser_set_duty_cycle(laser, duty_cycle);
			usleep(10);
		}
		for (duty_cycle = 1; duty_cycle >= 0; duty_cycle -= step) {
			laser_set_duty_cycle(laser, duty_cycle);
			usleep(10);
		}
	}

	// make the motor 2 successively move faster.
	float s;
	for (s = 1; s < 0.01; s -= 0.5) {
		stepper_move(motor2, CW, s, 15);
		usleep(10);
	}

	// turn on LED.
	led_enable(led);

	// 3 times
	for (i = 0; i < 3; i++) {

		// fade led on and off
		float duty_cycle;
		float step = 0.0005;
		for (duty_cycle = 0; duty_cycle <= 1; duty_cycle += step) {
			led_set_duty_cycle(led, duty_cycle);
			usleep(10);
		}
		for (duty_cycle = 1; duty_cycle >= 0; duty_cycle -= step) {
			led_set_duty_cycle(led, duty_cycle);
			usleep(10);
		}
	}

	// delete stuff
	laser_disable(laser);
	laser_delete(laser);

	led_disable(led);
	led_delete(led);

	stepper_delete(motor1);
	stepper_delete(motor2);
}
