#include "mraa/aio.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * Lab 2 - accelX.c
 * 
 * Print out normalized values from the acellerometer from input
 * data from the x direction.
 */
int accelX(){
	// max Arduino voltage = 3.3 V
	// documentation states that at 3.6V the output sensitivity is 360 mV/g
	// I can infer that for 3.3V the output sensitivity is approximately 3.3 mV/g.
	float mv_per_g = 3.3;

	// calibrating values (measured via testing)
	unsigned int x_up = 293;    // +1g
	unsigned int x_down = 430;  // -1g
	unsigned int x_base = 366;  //  0g 
	
	// range is down - up
	unsigned int x_range = x_down - x_up;
	
	// the difference in x_up and x_down divided by 2 should be g. (ignoring directions)
	float x_g = (x_range) / 2.0;

	int i = 0;
	while(true){
		mraa_aio_context x_pin = mraa_aio_init(0);
		unsigned int x_input_raw = mraa_aio_read(x_pin);
		
		// units are in gs, as you have bits / (bits per g), or bits * (g / bits), which is in terms of g.
		float x_g_normalized = (x_input_raw - x_base) / (x_g);
		printf("%3d X Axis Accelerometer Values:   hex: %#8x  int: %3d  float: %.3f\n", i, x_input_raw, x_input_raw, x_g_normalized);
		mraa_aio_close(x_pin);
		usleep(200000);
		i++;
	}

	return MRAA_SUCCESS;
}

int main(){
	return accelX();
}