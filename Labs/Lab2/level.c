#include "mraa/aio.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * Lab 2 - level.c
 * 
 * Light up none, one or both LEDs based on how level the 
 * accelerometer is.  This implementation considers level as if
 * the accelerometer is laying with it's x and y axes running
 *  parallel with the Earth's surface.
 */
int level()
{
	// scaling factor and base measurements based on previous measurements
	float bits_per_g = (430 - 293) / 2.0;
	unsigned int bits_base = 366;

	// our x and y pins
	mraa_aio_context x_pin;
	mraa_aio_context y_pin;
	mraa_gpio_context red_led_pin;
	mraa_gpio_context yellow_led_pin;

	// some local
	const float one_degree = (M_PI / 180) * 1;
	const float ten_degrees = (M_PI / 180) * 10;

	// assign pins
	x_pin = mraa_aio_init(0);
	y_pin = mraa_aio_init(1);

	// setup LED pins
	red_led_pin = mraa_gpio_init(7);
	yellow_led_pin = mraa_gpio_init(8);

	// set both pins to outputs
	mraa_gpio_dir(red_led_pin, MRAA_GPIO_OUT);
	mraa_gpio_dir(yellow_led_pin, MRAA_GPIO_OUT);

	for(;;) {

		// read raw int values from pins
		unsigned int x_input_raw = mraa_aio_read(x_pin);
		unsigned int y_input_raw = mraa_aio_read(y_pin);

		/*
		 * convert raw inputs to radians by normalizing the int values in terms
		 * of g (since valid inputs are ranged from -1 to 1 g's), then taking the
		 * arcsign of the absolute value of that result, since we don't care about
		 * the directional information.
		 */
		float x_radians = asin(fabs( (x_input_raw - bits_base) / bits_per_g));
		float y_radians = asin(fabs( (y_input_raw - bits_base) / bits_per_g));
		printf("x_radians: %10f   y_radians: %10f   - ", x_radians, y_radians);

		if (x_radians > ten_degrees || y_radians > ten_degrees)
		{
			// turn LEDS off
			printf("both LEDS off\n");
			mraa_gpio_write(red_led_pin, 0);
			mraa_gpio_write(yellow_led_pin, 0);

		}
		else if (x_radians <= one_degree || y_radians <= one_degree)
		{
			// turn both LEDs on
			printf("both LEDS on\n");
			mraa_gpio_write(red_led_pin, 1);
			mraa_gpio_write(yellow_led_pin, 1);

		}
		else // in the final case, both axes are between 1 degrees and 10 degrees.
		{
			// turn the yellow LED on, red LED off
			printf("turn yellow LED on\n");
			mraa_gpio_write(red_led_pin, 0);
			mraa_gpio_write(yellow_led_pin, 1);

		}

		// release memory

		//usleep(200000);
	}

	mraa_aio_close(x_pin);
	mraa_aio_close(y_pin);
	mraa_gpio_close(red_led_pin);
	mraa_gpio_close(yellow_led_pin);
	return 0;
}

int main(){
	return level();
}