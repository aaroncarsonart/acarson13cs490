#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "mraa.h"

/*
 * Lab 2 - blink.c
 * 
 * Blink the red and yellow LEDs as according to #1 of Lab 2.
 */
int blink_both_leds() {
	printf("blink_both_leds()\n");
	//goal: blink both LEDs

	printf("set up pins ...\n");
	// initialize context pins
	mraa_gpio_context red_led_pin = mraa_gpio_init(7);
	mraa_gpio_context yellow_led_pin = mraa_gpio_init(8);

	// set both pins to outputs
	mraa_result_t red_out_result = mraa_gpio_dir(red_led_pin, MRAA_GPIO_OUT);
	mraa_result_t yellow_out_result = mraa_gpio_dir(yellow_led_pin,
			MRAA_GPIO_OUT);

	// error checking
	if (red_out_result != MRAA_SUCCESS || yellow_out_result != MRAA_SUCCESS) {
		printf("error setting one of the pins to output, aborting.\n");
		return MRAA_ERROR_UNSPECIFIED;
	} else {
		printf("Successfully set pins 7 & 8 to MRAA_GPIO_OUT.\n");
	}

	// loop and blink stuff
	while (true) {
		// both off
		mraa_gpio_write(red_led_pin, 0);
		mraa_gpio_write(yellow_led_pin, 0);
		sleep(1);
		// red on, yellow off
		mraa_gpio_write(red_led_pin, 1);
		mraa_gpio_write(yellow_led_pin, 0);
		sleep(1);
		// red off, yellow on
		mraa_gpio_write(red_led_pin, 0);
		mraa_gpio_write(yellow_led_pin, 1);
		sleep(1);
		// red on, yellow on
		mraa_gpio_write(red_led_pin, 1);
		mraa_gpio_write(yellow_led_pin, 1);
		sleep(1);
	}
	return MRAA_SUCCESS;
}

int main() {
	return blink_both_leds();
}
